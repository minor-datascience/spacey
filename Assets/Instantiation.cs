﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instantiation : MonoBehaviour
{
    // Reference to the Prefab. Drag a Prefab into this field in the Inspector.
    public GameObject rocketEnv;
    public int width = 4;
    public int height = 8;
    public int inBetweenDistance = 200;

    // This script will simply instantiate the Prefab when the game starts.
    void Start()
    {
        for (int i = 0; i < width; i++)
        {
            for (int x = 0; x < height; x++)
            {
                Instantiate(rocketEnv, new Vector3(i * inBetweenDistance, 0, x * inBetweenDistance), Quaternion.identity);
            }
        }
        // Instantiate at position (0, 0, 0) and zero rotation.
        
    }
}


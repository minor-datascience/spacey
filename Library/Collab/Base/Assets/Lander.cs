﻿using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;

public class Lander : Agent
{
    public Rigidbody rocket_rigidbody;
    public GameObject rocket;
    public Rigidbody booster1;
    public Rigidbody booster2;
    public Rigidbody booster3;
    public Rigidbody booster4;
    public GameObject target;
    public float starting_Velocity = 5;   

    public override void OnEpisodeBegin()
    {
        //add rotation
        // float range = Random.Range(-5.0f, 5.0f);
        // Vector3 rotation_and_position = new Vector3(range,range,range);
        // rocket.transform.Rotate(rotation_and_position);
        // rocket.transform.position += rotation_and_position;
        // rocket_rigidbody.velocity = new Vector3(0, -starting_Velocity, 0);
    }

    public override void CollectObservations(VectorSensor sensor){
        sensor.AddObservation(rocket.transform.rotation.x);
        sensor.AddObservation(rocket.transform.rotation.y);
        sensor.AddObservation(rocket.transform.rotation.z);
        
        sensor.AddObservation(rocket.transform.position.x);
        sensor.AddObservation(rocket.transform.position.y);
        sensor.AddObservation(rocket.transform.position.z);

        sensor.AddObservation(rocket_rigidbody.velocity.x);
        sensor.AddObservation(rocket_rigidbody.velocity.y);
        sensor.AddObservation(rocket_rigidbody.velocity.z);
    }
    
    public float forceMultiplier = 10f;
    public override void OnActionReceived(float[] vectorAction)
    {   
        // Debug.Log("Received booster 1: " + vectorAction[0]);
        // Debug.Log("Received booster 2: " + vectorAction[1]);
        // Debug.Log("Received booster 3: " + vectorAction[2]);
        // Debug.Log("Received booster 4: " + vectorAction[3]);

        booster1.AddRelativeForce(0, 0, -vectorAction[0] * forceMultiplier);
        booster2.AddRelativeForce(0, 0, vectorAction[1] * forceMultiplier);
        booster3.AddRelativeForce(-vectorAction[2] * forceMultiplier, 0, 0);
        booster4.AddRelativeForce(vectorAction[3] * forceMultiplier, 0, 0);

        // Rewards
        float distanceToTarget = Vector3.Distance(rocket.transform.position, target.transform.position);
        Debug.Log("Distance to target: " + distanceToTarget);
        // Reached target

        //When is it good?
        //Rotation less than 1 degree, velocity 0;
        //Distance to target
        Debug.Log("Rocket velocity: " + rocket_rigidbody.velocity.y);
    
        if(distanceToTarget < 4.5f &&
            rocket.transform.rotation.x < 0.2f && 
            rocket.transform.rotation.y < 0.2f && 
            rocket.transform.rotation.z < 0.2f &&
            rocket_rigidbody.velocity.y < 1.0f){
                Debug.Log("Reward!");
                SetReward(1.0f);
                EndEpisode();
        }else{
            Debug.Log("Big oof...");
            EndEpisode();
        }
            
        

        // Fell off platform
        if (rocket.transform.position.y < 2)
        {
           Debug.Log("Big oof...");
           EndEpisode();
        }
    }


    public override void Heuristic(float[] actionsOut)
    {
        if(Input.GetKey(KeyCode.A)){
            actionsOut[0] = 10.0f;
        }else{
            actionsOut[0] = 0f;
        }

        if(Input.GetKey(KeyCode.S)){
            actionsOut[1] = 10.0f;
        }else{
            actionsOut[1] = 0f;
        }

        if(Input.GetKey(KeyCode.D)){
            actionsOut[2] = 10.0f;
        }else{
            actionsOut[2] = 0f;
        }
          
        if(Input.GetKey(KeyCode.W)){
            actionsOut[3] = 10.0f;
        }else{
            actionsOut[3] = 0f;
        }
    
    }
}